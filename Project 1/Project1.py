#!/usr/bin/env python3
import glob
import numpy as np
import matplotlib.pyplot as plt
import cv2
import math
from scipy.ndimage import gaussian_filter1d

# Function that applies a temporal filter to array of images
# option is to choose a simple averaging filter or a gaussian filter
def tempFilter(images, option, tsigma = 0):
    filtered = []

    # the threshold is defined as the mean of the estimate acquisition noise
    threshold = 3*Threshold(images, 0.1, 0.8)
    #print(threshold)
    theshArray = threshold * np.ones((images.shape[1], images.shape[2]))

    n, row, col = np.shape(images)
    conv = np.zeros(images.shape)

    if option == 1:
        kernel = np.array([-1, 0, 1])/2
        for r in range(0, row-1):
            for c in range(0, col-1):
                # Apply threshold to the difference of images in time to get new frame
                conv[:,r,c] = np.abs(np.convolve(images[:,r,c].T, kernel, mode='same'))
        for i in conv:
            newFrame = np.greater(i, theshArray)
            filtered.append(newFrame)
            # filtered.append(i)
    elif option == 2:
        size = math.ceil(tsigma*5)
        if size % 2 == 0:
            size = size + 1
        kernel = np.matrix.transpose(cv2.getGaussianKernel(size, tsigma))
        kernel = np.gradient(kernel[0])

        for r in range(0, row-1):
            for c in range(0, col-1):
                # Apply threshold to the difference of images in time to get new frame
                conv[:,r,c] = np.abs(np.convolve(images[:,r,c].T, kernel, mode='same'))
        for i in conv:
            newFrame = np.greater(i, theshArray)
            filtered.append(newFrame)
            #filtered.append(i)
    else:
        print("Invalid option")

    return np.array(filtered, dtype='float16')

# Function that applies a spacial filter to an array of images
# option is 1: box filter size 3, 2: box filter size 5
# 3: gaussian with deviation sigma
def spacialFilter(images, option, sigma = 0):
    filtered = []

    images = images.astype(float)

    if option == 1:
        kernel3 = np.ones((3, 3), np.float32) / 9
        for img in images:
            filtered.append(np.array(cv2.filter2D(img, ddepth=-1, kernel=kernel3), dtype='uint8'))
    elif option == 2:
        kernel5 = np.ones((5, 5), np.float32) / 25
        for img in images:
            filtered.append(np.array(cv2.filter2D(img, ddepth=-1, kernel=kernel5), dtype='uint8'))
    elif option == 3:
        size = math.ceil(5*sigma)
        if size % 2 == 0:
            size = size + 1
        for img in images:
            filtered.append(np.array(cv2.GaussianBlur(img, (size, size), sigma), dtype='uint8'))
    else:
        print("Invalid option")

    return np.asanyarray(filtered)

### Threshold ################################################################
### 3x3 pixels from 4 corners of each image
# ps nd p2bare percentage of rows and columns to find corners
def Threshold(images, ps, pb):

    # Arrays containing corners
    UpperLeft = []
    UpperRight = []
    LowerLeft = []
    LowerRight = []

    # Arrays used to estimate mean noise of each corner
    E_UpperLeft = np.zeros((2, 2))
    E_UpperRight = np.zeros((2, 2))
    E_LowerLeft = np.zeros((2, 2))
    E_LowerRight = np.zeros((2, 2))

    # Definition of position of corners
    n ,row, column = images.shape
    xup = (int)(row* ps)
    xdown = (int)(row * pb)
    yright = (int)(column * pb)
    yleft = (int)(column * ps)

    i = 0
    for img in images:
        UpperLeft.append(img[xup:xup+2, yleft:yleft+2])
        E_UpperLeft = np.add(E_UpperLeft, UpperLeft[i])

        UpperRight.append(img[xup:xup+2,yright-2:yright])
        E_UpperRight = np.add(E_UpperRight, UpperRight[i])

        LowerLeft.append(img[xdown-2:xdown, yleft:yleft+2])
        E_LowerLeft = np.add(E_LowerLeft, LowerLeft[i])

        LowerRight.append(img[xdown-2:xdown, yright-2:yright])
        E_LowerRight = np.add(E_LowerRight, LowerRight[i])

        i += 1

    E_UpperLeft = E_UpperLeft / n
    E_UpperRight = E_UpperRight / n
    E_LowerLeft = E_LowerLeft / n
    E_LowerRight = E_LowerRight / n

    sig_UL = getSig(images, UpperLeft, E_UpperLeft)
    sig_UR = getSig(images, UpperRight, E_UpperRight)
    sig_LL = getSig(images, LowerLeft, E_LowerLeft)
    sig_LR = getSig(images, LowerRight, E_LowerRight)

    sigmaNoise = np.mean([sig_UL, sig_UR, sig_LL, sig_LR])
    return sigmaNoise

# Function to get mean noise
def getSig(im, corner, cornes_sum):
    rezult = (corner - cornes_sum) ** 2
    rezult = rezult.sum(axis=0)
    # print(rezult)
    sig = np.sqrt((1 / (len(im) - 1)) * rezult)
    sig = np.mean(sig)
    return sig

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])

def main():
    mDir = "/home/choro/Desktop/Computer Vision/Project 1/"
    # Folders for videos
    folders = ["EnterExitCrossingPaths2cor/EnterExitCrossingPaths2cor/EnterExitCrossingPaths2cor",
                        "Office/Office/img",
                        "RedChair/RedChair/advbgst1_"]

    # Do for each video in designated folder
    for f in folders:
        print('Reading images from folder:', f.split('/')[0], '...')
        d = mDir + f + "*.jpg"
        filenames = glob.glob(d)
        filenames.sort()
        # Save images to array
        images = np.array([rgb2gray(cv2.imread(file)) for file in filenames], dtype='uint8')

        print('\nComputing temporal filters...')
        tfltrBox = tempFilter(images, 1)
        tsigma = .6
        tfltrGaus = tempFilter(images, 2, tsigma)

        print('\nDisplaying result of temporal filters...')
        # Part b (i)
        try:
            # Show temporal filter
            for (img, imB, imG) in zip(images, tfltrBox, tfltrGaus):
                cv2.imshow('Input images', img/255)
                cv2.imshow('Box Temporal Filter', imB.astype(float))
                cv2.imshow('Gaussian Temporal Filter', imG.astype(float))
                cv2.waitKey(50)
        except KeyboardInterrupt:
            pass
        cv2.destroyAllWindows()

        # part b (ii)
        # 2D Spacial filtering
        ssigma = 2

        print('\nApplying spacial filters...')
        smooth3 = spacialFilter(images, 1)
        smooth5 = spacialFilter(images, 2)
        smoothG = spacialFilter(images, 3, ssigma)

        print('\nApplying temporal filters...')
        tfltrBox2 = tempFilter(smooth3, 1)
        tfltrGaus2 = tempFilter(smooth3, 2, tsigma)
        tfltrBox3 = tempFilter(smooth5, 1)
        tfltrGaus3 = tempFilter(smooth5, 2, tsigma)
        tfltrBox4 = tempFilter(smoothG, 1)
        tfltrGaus4 = tempFilter(smoothG, 2, tsigma)



        # # 50 , 500, 30
        # frame = 500
        # cv2.imshow('Input images',smooth3[frame] / 255)
        # cv2.imshow('Box Temporal Filter', tfltrBox2[frame].astype(float))
        # cv2.imshow('Gaussian Temporal Filter', tfltrGaus2[frame].astype(float))
        #
        # cv2.imshow('Input images2',smooth5[frame] / 255)
        # cv2.imshow('Box Temporal Filter2', tfltrBox3[frame].astype(float))
        # cv2.imshow('Gaussian Temporal Filter2', tfltrGaus3[frame].astype(float))
        #
        # cv2.imshow('Input images3',smoothG[frame] / 255)
        # cv2.imshow('Box Temporal Filter3', tfltrBox4[frame].astype(float))
        # cv2.imshow('Gaussian Temporal Filter3', tfltrGaus4[frame].astype(float))
        # cv2.waitKey()

        print('\nDisplaying results of filtering...')
        try:
            # Show output of 2D Box filter 3x3
            for (i3Box, i3Gaus) in zip(tfltrBox2,tfltrGaus2):
                cv2.imshow('Box temporal filter after 3x3 box', i3Box.astype(float))
                cv2.imshow('Gauss temporal filter after 3x3 box', i3Gaus.astype(float))
                cv2.waitKey(50)
        except KeyboardInterrupt:
            pass
        cv2.destroyAllWindows()

        try:
            # Show output of 2D Box filter 3x3
            for (i3Box, i3Gaus) in zip(tfltrBox3,tfltrGaus3):
                cv2.imshow('Box temporal filter after 5x5 box', i3Box.astype(float))
                cv2.imshow('Gauss temporal filter after 5x5 box', i3Gaus.astype(float))
                cv2.waitKey(50)
        except KeyboardInterrupt:
            pass
        cv2.destroyAllWindows()

        try:
            # Show output of 2D Box filter 3x3
            for (i3Box, i3Gaus) in zip(tfltrBox4,tfltrGaus4):
                cv2.imshow('Box temporal filter after spacial gaussian', i3Box.astype(float))
                cv2.imshow('Gauss temporal filter after spacial gaussian', i3Gaus.astype(float))
                cv2.waitKey(50)
        except KeyboardInterrupt:
            pass
        cv2.destroyAllWindows()

        print('\nFinished with folder:', f , '!\n')

if __name__ == '__main__':
    main()